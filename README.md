# Workshop de Integración Continua

## Temas principales 😎

- Control de versiones
- Herramientas de construcción
- Triggers de ejecución

## Temas previos a CI

- Pruebas
- Shell script
- Gradle
